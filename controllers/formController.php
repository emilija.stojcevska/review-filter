<?php

 include ("FilterController.php");

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

   
    $filterController = new FilterController;
    $result = $filterController->results($_POST);
  
    if($result['error']){
      
        header('Location:../index.php?error=error');
        exit;
    }else{
        header('Location:../results_page.php');
        exit; 
    }
 

}