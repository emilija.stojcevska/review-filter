<?php
    include('ApiController.php');


class FilterController extends ApiController{
  
    protected $resArr;

    public function results($filters){

       
        $api = new ApiController;
        $rating = $filters['rating'];
        $min_rating = $filters['min_rating'];
        $date = $filters['date'];
        $by_text = $filters['by_text'];

        $api_results = $api->callApi();
        $this->resArr = array();
        $this->resArr = json_decode($api_results, true);
      
       
        if($this->resArr['success']){
         
            
            if($this->resArr['reviews']){
               $this->resArr = $this->resArr['reviews'];
            }

            if(isset($min_rating)){
                $whatToFilter = 'rating';
                $array = $this->filterByMinReting($min_rating, $whatToFilter);
            }
         
            if(isset($rating)){
                if($rating == "desc"){
                    usort($this->resArr, array($this, "filterByRatingDesc"));
                }else{
                    usort($this->resArr, array($this, "filterByRatingAsc"));
                }   
            }
 
            if(isset($date)){
                if($date == 'desc'){
                    $this->filterByDateDesc();  
                }else{
                    $this->filterByDateAsc();
                }
            }

            if($by_text == 'yes'){
                
               $result = $this->filterByText();  
            } 
          
            session_start();
           
            $_SESSION['results'] = $this->resArr;

            header('Location:../results_page.php');
            exit; 
            
        }
        
        return ['error' => 'error'];
    }

    public function filterByRatingDesc($a, $b){
        return $a['rating'] < $b['rating'] ? 1 : -1;
    }
    public function filterByRatingAsc($a, $b){   
        return $a['rating'] > $b['rating'] ? 1 : -1;
    }

    public function filterByDateDesc(){
        $arrayMerge = [];
        $i = 0;
        foreach($this->resArr as $result){
            $element = $result['rating'];
           if($element == $i){
               continue;
           }else{
                $arrayNew = [];
                foreach($this->resArr as $newresult){
                
                    if($newresult['rating'] == $element){
                        $i = $element;
                        $arrayNew[] = $newresult;
                    }
                }
                usort( $arrayNew, array($this, "filterByDateDescArr"));
            }
            $arrayMerge = array_merge( $arrayMerge, $arrayNew);
        }
        $this->resArr =  $arrayMerge;
    }
    public function filterByDateDescArr($a, $b){
       
        return (strtotime($a['reviewCreatedOnDate']) < strtotime($b['reviewCreatedOnDate'])) ? 1 : -1;
        
    }
    public function filterByDateAsc(){
        $arrayMerge = [];
        $i = 0;
        foreach($this->resArr as $result){
            $element = $result['rating'];
           if($element == $i){
               continue;
           }else{
                $arrayNew = [];
                foreach($this->resArr as $newresult){
                
                    if($newresult['rating'] == $element){
                        $i = $element;
                        $arrayNew[] = $newresult;
                    }
                }
                usort( $arrayNew, array($this, "filterByDateAscArr"));
            }
            $arrayMerge = array_merge( $arrayMerge, $arrayNew);
        }
        $this->resArr =  $arrayMerge;
    }
    
    public function filterByDateAscArr($a, $b){
        return (strtotime($a['reviewCreatedOnDate']) > strtotime($b['reviewCreatedOnDate'])) ? 1 : -1;    
    } 

    public function filterByText(){
        $arrayText = array();
        $arrayNonText = array();
        foreach($this->resArr as $result){
            if($result['reviewText'] == ""){
                $arrayNonText[] = $result;
            }else{
                $arrayText[] = $result;
            }
        }

        $array = array_merge($arrayText, $arrayNonText);
      
        $this->resArr = $array;
        return true;
    }

   

    public function filterByMinReting($rating, $whatToFilter ){
        foreach($this->resArr as $kay => $result){
            
            if($result["{$whatToFilter}"] < $rating){
                unset($this->resArr[$kay]);
            }
           
        }
        return true;
    }
}