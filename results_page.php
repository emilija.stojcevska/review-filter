<?php 
session_start();
if(isset($_SESSION['results'])){
    $results = $_SESSION['results'];
}else{
    header('Location:index.php?error=error');
    exit;
}
include('partials/header.php');?>

<div class="container">
<div class="row">
<div class="col-6 offset-2 mt-5">
<div class="d-flex justify-content-between">
  <h2 class="mb-2">Results: </h2>
  <a href="index.php" class="btn btn-primary">Back to filters</a>
</div>
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Review Text</th>
      <th scope="col">Rating</th>
      <th scope="col">Review Created On Date</th>

    </tr>
  </thead>
  <tbody>
  <?php if(isset($results)): 
            if(count($results) > 0):
                $i = 0;
            foreach($results as $result):
            $i++;
            ?>
    <tr>
      <td><?= $i;?>.</td>
      <td><?= $result['reviewText'];?></td>
      <td><?= $result['rating'];?></td>
       
       <td><?= date('d-m-Y H:i:s', strtotime($result['reviewCreatedOnDate']));?></td> 
      
    </tr>

    <?php endforeach;
         endif;
        endif;
    ?>
    
  </tbody>
</table>

</div>
</div>
</div>


<?php
  session_destroy();
 include('partials/footer.php');?>