
<?php
include('partials/header.php');
session_start();
?>


<div class="container">

    <div class="row">.
        <?php if(isset($_GET['error'])):?>

            <div class="alert alert-warning alert-dismissible fade show" role="alert">
            Something went wrong. Please try again later
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php endif;?>
        <div class="col-6 offset-2 mt-5">
            <h1> Filter reviews</h1>
            <form action="controllers/formController.php" method="POST">
                <div class="mb-3">
                    <label for="rating" class="form-label">Order by rating</label>
                    <select class="form-select" aria-label="Default select example" id="rating" name="rating">
                        <option selected value="desc">Highest first</option>
                        <option value="asc">Lowest first</option>
                        
                    </select>
                </div>
                <div class="mb-3">
                    <label for="min_rating" class="form-label">Minimum rating</label>
                    <select class="form-select" aria-label="Default select example" id="min_rating" name="min_rating">
                    
                        <option selected  value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="date" class="form-label">Order by date</label>
                    <select class="form-select" aria-label="Default select example" id="date" name="date">
                        <option selected value="desc">Newest First</option>
                        <option value="1" value="asc"> Oldest First </option>
                    
                    </select>
                </div>
                <div class="mb-3">
                    <label for="by_text" class="form-label">Prioritize by text</label>
                    <select class="form-select" aria-label="Default select example" id="by_text" name="by_text">
                        <option selected value="yes">Yes</option>
                        <option value="no">No</option>
                    
                    </select>
                </div>
                <div class="mb-3">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

        </div>

    </div>

</div>



<?php  session_destroy(); include('partials/footer.php');?>
    
